package src;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Set;
import java.util.TreeSet;

public class Parser {

    public String readFile(String path) throws IOException {

        Reader rd = new FileReader(path);
        StringBuffer sb = new StringBuffer();
        int c;

        while ((c = rd.read()) != -1) {
            sb.append((char) c);
        }

        rd.close();

        return sb.toString();
    }

    public Set transformation(String text) {

        Set<String> singleWords = new TreeSet();
        String splitWords[] = text.split("[ ,.•()/:;\\-\n\r]");

        for (int i = 0; i < splitWords.length; i++) {
            if (!splitWords[i].equals("")) {
                singleWords.add(splitWords[i].toLowerCase());
            }
        }

        return singleWords;
    }


}
