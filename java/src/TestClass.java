package src;

import java.io.File;
import java.io.IOException;

public class TestClass {

    public static void main(String[] args) throws IOException {

        File file1 = new File("D:\\IntelliJ IDEA 14.0.2\\ArtCode2\\src\\TestWork\\java\\myresume.txt");
        File file2 = new File("D:\\IntelliJ IDEA 14.0.2\\ArtCode2\\src\\TestWork\\java\\job.txt");
        Parser parse1 = new Parser();
        Parser parse2 = new Parser();
        Calculating calculating = new Calculating();

        String text1 = parse1.readFile(file1.getPath());
        String text2 = parse2.readFile(file2.getPath());

        System.out.println("Your document: "+ file1.getName()+"\n"+"Reference document: "+ file2.getName()+"\n"+"Analysis:");
        System.out.println(calculating.compare(parse2.transformation(text2), parse1.transformation(text1))+"% of words in "+file1.getName()+" are in "+ file2.getName());
        System.out.println(calculating.compare(parse1.transformation(text1), parse2.transformation(text2))+"% of words in "+file2.getName()+" are in "+ file1.getName());
        System.out.println("Done");

    }

}
