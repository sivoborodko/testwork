package src;

import java.util.Iterator;
import java.util.Set;

public class Calculating {

    public String compare(Set o1, Set o2) {

        Iterator itr = o2.iterator();
        int count = 0;

        while (itr.hasNext()) {

            if (o1.contains(itr.next())) {
                count++;
            }
        }

        return String.format("%.2f", (double)count*100/ o2.size());

    }

}
